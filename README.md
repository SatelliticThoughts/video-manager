# Video Manager

Allows playlist creation and random video selection.


## Preview

![](preview/preview.png)


## License

![GPLv3](LICENSE)
