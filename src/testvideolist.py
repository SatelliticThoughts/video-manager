#!/usr/bin/env python3


import unittest
from videolist import VideoList


class TestVideoListGetFiles(unittest.TestCase):
    def test_get_files_empty_files(self):
        vl = VideoList()
        self.assertEquals(vl.get_files(), [])

    def test_get_files_populated_files(self):
        files = ["test1", "test2"]
        vl = VideoList(files=files)
        self.assertEquals(vl.get_files(), files)

    def test_get_files_not_equal(self):        
        files = ["test1", "test2"]
        vl = VideoList()
        self.assertNotEquals(vl.get_files(), files)


class TestVideoListSetFiles(unittest.TestCase):
    def test_set_files(self):
        files = ["test1", "test2"]
        vl = VideoList()
        vl.set_files(files)
        self.assertEquals(vl.get_files(), files)
    
    def test_set_files_not_equal(self):
        f1 = ["test1", "test2"]
        f2 = ["test2", "test3"]
        vl = VideoList()
        vl.set_files(f1)
        self.assertNotEquals(vl.get_files(), f2)


class TestVideoListGetRandomFile(unittest.TestCase):
    def test_get_random_file(self):
        paths = ["/Test/1", "/Test/2"]
        files = [{"name": "Test1", "path": paths[0]}, {"name": "Test2", "path": paths[1]}]
        vl = VideoList(files=files)
        self.assertTrue(vl.get_random_file() in paths)

    def test_get_random_file_false(self):
        p1 = ["/Test/1", "/Test/2"]
        p2 = ["/Test/3", "/Test/4"]
        files = [{"name": "Test1", "path": p1[0]}, {"name": "Test2", "path": p1[1]}]
        vl = VideoList(files=files)
        self.assertFalse(vl.get_random_file() in p2)


class TestVideoListLoadFiles(unittest.TestCase):
    def test_load_files(self):
        folder = "/home/thoughts/Videos/Random"
        videos = ["movie.mp4", "movie.webm", "movie.wav", 
                "tvshow1.mp4", "tvshow2.mp4", "tvshow3.mp4"]
        i = 0
        for video in videos:
            videos[i] = {"name": video, "path": folder + "/" + video}
            i += 1

        vl = VideoList()
        vl.load_files(folder)
        for video in vl.get_files():
            self.assertTrue(video in videos)

    def test_load_files_false(self):
        folder = "/home/thoughts/Videos/Random"
        videos = ["movie0.mp4", "movie0.webm", "movie0.wav", 
                "tvshow10.mp4", "tvshow20.mp4", "tvshow30.mp4"]
        i = 0
        for video in videos:
            videos[i] = {"name": video, "path": folder + "/" + video}
            i += 1

        vl = VideoList()
        vl.load_files(folder)
        for video in vl.get_files():
            self.assertFalse(video in videos)


class TestVideoListGetFilename(unittest.TestCase):
    def test_get_filename(self):
        path = "/test/file.mp4"
        filename = "file.mp4"

        vl = VideoList()
        self.assertEquals(vl.get_filename(path), filename)

    def test_get_filename_not_equal(self):
        path = "/test/file.mp4"
        filename = "file0.mp4"

        vl = VideoList()
        self.assertNotEquals(vl.get_filename(path), filename)


if __name__ == '__main__':
    unittest.main()