#!/usr/bin/env python3


import subprocess

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from videolist import VideoList
from jsonhandler import JsonHandler


class WinMain:
    def __init__(self):
        self.__v_list = VideoList()

        self.__builder = Gtk.Builder()
        self.__builder.add_from_file("winmain.glade")

        self.__win = self.__builder.get_object("window")
        self.__win.connect("destroy", Gtk.main_quit)

        self.__entry = self.__builder.get_object("entry")

        self.__chooser = self.__builder.get_object("chooser")
        self.__chooser.connect("file-set", self.on_file_set)

        self.__listbox = self.__builder.get_object("listbox")

        self.__btn_random = self.__builder.get_object("btnRandom")
        self.__btn_random.connect("clicked", self.on_btn_random_clicked)

        self.__btn_load = self.__builder.get_object("btnLoad")
        self.__btn_load.connect("clicked", self.on_btn_load_clicked)

        self.__btn_save = self.__builder.get_object("btnSave")
        self.__btn_save.connect("clicked", self.on_btn_save_clicked)

        self.__btn_open = self.__builder.get_object("btnOpen")
        self.__btn_open.connect("clicked", self.on_btn_open_clicked)

        self.__btn_clear = self.__builder.get_object("btnClear")
        self.__btn_clear.connect("clicked", self.on_btn_clear_clicked)

    def display(self):
        self.__win.show_all()

    def on_file_set(self, chooser):
        self.__entry.set_text(self.__chooser.get_filename())

    def on_btn_random_clicked(self, btn):
        if self.__v_list.get_files():
            subprocess.run(["xdg-open", self.__v_list.get_random_file()])

    def on_btn_load_clicked(self, btn):
        self.__v_list.load_files(self.__entry.get_text())

        self.load_rows(self.__v_list.get_files())

    def on_btn_save_clicked(self, button):
        jh = JsonHandler()
        jh.save_playlist(self.__v_list.get_files())

    def on_btn_open_clicked(self, button):
        jh = JsonHandler()

        self.__v_list.set_files(jh.get_playlist())

        self.load_rows(self.__v_list.get_files())

    def on_btn_clear_clicked(self, button):
        self.__v_list.set_files([])
        for row in self.__listbox.get_children():
            self.__listbox.remove(row)

    def add_row(self, title):
        row = Gtk.ListBoxRow()
        box = Gtk.Box()
        box.set_margin_left(12)
        box.set_margin_top(6)
        box.set_margin_bottom(6)
        box.add(Gtk.Label(title))
        row.add(box)
        self.__listbox.add(row)

    def load_rows(self, files):
        for file in files:
            self.add_row(file["name"])
        self.__listbox.show_all()
