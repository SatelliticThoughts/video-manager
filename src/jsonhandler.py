#!/usr/bin/env python3


import json
import os


class JsonHandler:
    def __init__(self):
        self.__file = "playlist.json"
        if not os.path.isfile(self.__file):
            self._create_playlist()

    def _create_playlist(self):
            f = open(self.__file, "w+")
            f.close()

    def get_playlist(self):
        f = open(self.__file, "r")
        data = f.read()
        f.close()
        return json.loads(data)

    def save_playlist(self, data):
        f = open(self.__file, "w")
        f.write(json.dumps(data))
        f.close()
