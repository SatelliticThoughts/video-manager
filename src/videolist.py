#!/usr/bin/env python3


from glob import glob
import random


class VideoList:
    def __init__(self, files=[]):
        self.set_files(files)
        self.__types = ["/*.mkv", "/*.mp4", "/*.wav", "/*.webm"]

    def load_files(self, folder):
        files = []

        for type in self.__types:
            for path in glob(folder + type):
                files.append({"name": self.get_filename(path), "path": path})

        self.set_files(files)

    def get_random_file(self):
        choice = random.randint(0, len(self.__files) - 1)
        return self.__files[choice]["path"]

    def get_files(self):
        return self.__files

    def set_files(self, files):
        self.__files = files

    def get_filename(self, path):
        return path.split("/")[-1]
