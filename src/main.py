#!/usr/bin/env python3


import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from winmain import WinMain


def main():
    win_main = WinMain()
    win_main.display()
    Gtk.main()


if __name__ == "__main__":
    main()
